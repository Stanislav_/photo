var express = require('express'),
    Album = require('../models/album'),
    router = express.Router();


router.get('/', function (req, res, next) {
    //if (req.session.passport.user) {
        Album.find({user: req.session.passport.user}, function (err, album) {
            if (err) {
                return next(err)
            }

            res.send(album);
        });
    //} else {
    //    Album.find({reportName: req.reportName}, function (err, album) {
    //        if (err) {
    //            return next(err)
    //        }
    //
    //        res.send(album);
    //    });
    //}
});

router.get('/:id/', function (req, res, next) {
    Album.findOne({_id: req.params.id}, function (err, album) {
        if (err) {
            return next(err)
        }

        res.send(album);
    });
});

router.post('/', function (req, res, next) {
    var album = req.body;
    album.user = req.session.passport.user;
    Album.create(album, function (err) {
        if (err) {
            return next(err)
        }

        res.send({success: true, msg: 'Album is added.'});
    });
});

router.delete('/', function (req, res, next) {
    Album.findOneAndRemove({_id: req.query._id}, function (err) {
        if (err) {
            return next(err)
        }

        res.send({success: true, msg: 'Album is deleted'});
    });
});

router.put('/:id', function (req, res, next) {
    Album.findOne({_id: req.body._id}, function (err, album) {
        if (err) {
            return next(err)
        }

        album.title = req.body.title;
        album.description = req.body.description;
        album.save();

        res.send({success: true, msg: 'Album is updated.'});
    });
});

module.exports = router;