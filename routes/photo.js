var express = require('express'),
    Photo = require('../models/photo'),
    User = require('../models/user'),
    multiparty = require('multiparty'),
    util = require('util'),
    fs = require('fs-extra'),
    router = express.Router();

router.get('/', function(req, res, next) {
    if (req.query.userId) {

        Photo.find({user: req.session.passport.user}, function (err, photo) {
            if(err) {
                return next(err)
            }

            res.send(photo);
        });

    } else if (req.query.albumId) {
        Photo.find({album: req.query.albumId}, function (err, photo) {
            if(err) {
                return next(err)
            }

            res.send(photo);
        });
    } else {
        Photo.find({reportName: req.reportName}, function (err, photo) {
            if(err) {
                return next(err)
            }
            res.send(photo);
        });
    }
});

router.post('/upload', function(req, res, next) {
    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files) {
        res.writeHead(200, {'content-type': 'text/plain'});
        res.write('received upload:\n\n');
        var title = Date.now() + '.' + fields.extension[0];
        fs.move(files.file[0].path, './public/img/uploads/' + title, function (err) {
            if (err) return console.error(err);

            var newPhoto = {};
            newPhoto.title = title;
            newPhoto.album = fields.album[0];
            newPhoto.user = req.session.passport.user;
            newPhoto.description = fields.description[0];
            newPhoto.created = Date.now();
            newPhoto.lastEdit = Date.now();
            if (fields.status) newPhoto.status = fields.status[0];

            Photo.create(newPhoto, function (err) {
                if(err) {
                    return next(err)
                }
            });
        });
        res.end(util.inspect({fields: fields, files: files}));
    });
});

router.post('/avatar', function(req, res, next) {
    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files) {
        res.writeHead(200, {'content-type': 'text/plain'});
        res.write('received upload:\n\n');
        var extension = files.file[0].originalFilename.split('.').pop().toLowerCase();
        var title = Date.now() + '.' + extension;
        fs.move(files.file[0].path, './public/img/avatars/' + title, function (err) {
            if (err) return console.error(err);

            User.findOne({ _id: req.session.passport.user }, function (err, user){
                if(err) return next(err);
                 if (user.avatar) {
                    fs.remove('./public/img/avatars/' + user.avatar, function (err) {
                        if (err) return console.error(err);
                    });
                 }

                user.avatar = title;
                user.save();
            })
        });
        res.end(util.inspect({fields: fields, files: files}));
    });
});

router.delete('/', function(req, res, next) {
    Photo.findOneAndRemove({ _id: req.query._id }, function(err) {
        if(err) return next(err);

        fs.remove('./public/img/uploads/' + req.query.title, function (err) {
            if (err) return console.error(err);

            res.send({success: true});
        });
    });
});

router.put('/:id', function(req, res, next) {
    Photo.findOne({ _id: req.params.id }, function (err, photo){
        if(err) return next(err);

        if (req.body.comment) {
            User.findOne({ _id: req.session.passport.user }, function (err, user){
                if(err) return next(err);

                var comment = {};
                comment.body = req.body.comment;
                comment.date = Date.now();
                comment.user = user.username;

                photo.comments.push(comment);
                photo.save();

                res.send({success: true});
            })
        } else {
            photo.album = req.body.album;
            photo.description = req.body.description;
            photo.status = req.body.status;
            photo.lastEdit = Date.now();
            photo.save();

            res.send({success: true});
        }
    });
});

module.exports = router;