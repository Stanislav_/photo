var express = require('express'),
    passport = require('passport'),
    User = require('../models/user'),
    LocalStrategy = require('passport-local').Strategy,
    router = express.Router(),
    bcrypt = require('bcryptjs');

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.getUserByUsername(username, function (err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, {message: 'Unknown User'});
            }

            User.comparePassword(password, user.password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, {message: 'Invalid password'});
                }
            });

        });

    }
));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.getUserById(id, function (err, user) {
        done(err, user);
    });
});

router.get('/', ensureAuthenticated, function (req, res, next) {

    User.find({reportName: req.reportName}, function (err, users) {
        if (err) {
            return next(err)
        }

        console.log(users);
        res.send(users);
    });
});

/* Register user*/
router.post('/register', function (req, res) {
    var name = req.body.name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var password2 = req.body.password2;

    // validation
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not required').isEmail();
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

    var errors = req.validationErrors();

    if (errors) {
        res.send({error: true, msg: 'Fill the fields correctly!'});
    } else {
        var newUser = new User({
            name: name,
            email: email,
            username: username,
            password: password,
            //isAdmin: true
        });

        User.createUser(newUser, function (err, user) {
            if (err) throw err;
            res.send({success: true, msg: 'You are registered and can now login.'});
        });
    }
});

router.post('/login',
    passport.authenticate('local', {successRedirect: '/', failureRedirect: '/#!/login', failureFlash: true}),
    function (req, res) {
        req.flash('success_msg', 'You are logged in');
        res.redirect('/');
    });

router.get('/logout', function (req, res) {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/');
});


router.get('/usersListView', ensureAuthenticated, function (req, res, next) {

    User.find({reportName: req.reportName}, function (err, users) {
        if (err) {
            return next(err)
        }
        res.send(users);
    });
});


/* Users profile */
router.get('/userProfile', function (req, res, next) {
    console.log('user profile');
    console.log(req.user);
    User.findOne({_id: req.user._id}, function (err, user) {
        if (err) {
            return next(err)
        }
        user.password = null;
        console.log(user);

        res.send(user);
    });
});

router.put('/userProfile', function (req, res, next) {
    User.findOne({_id: req.user._id}, function (err, user) {
        if (err) {
            return next(err)
        }
        console.log(req.body.name);

        user.username = req.body.username;
        user.email = req.body.email;
        user.name = req.body.name;
        if (req.body.password != null) {
            bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(req.body.password, salt, function (err, hash) {
                    user.password = hash;
                    user.save(function () {
                        res.send('Update user');
                    });
                });
            });
        } else {
            user.save(function () {
                res.send('Update user');
            });
        }


    });
});


router.delete('/', function (req, res, next) {
    User.findOneAndRemove({_id: req.query._id}, function (err) {
        if (err) return next(err);

        res.send({success: true});
    });
});


function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        req.flash('error_msg', 'You are not logged in');
        res.redirect('/#!/login');
    }
}

module.exports = router;