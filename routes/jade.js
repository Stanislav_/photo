var express = require('express');
var router = express.Router();

router.get('/*', function(req, res) {
    var template = req.originalUrl.replace('/view/', '').replace('.html', '');
    res.render(template);
});

module.exports = router;