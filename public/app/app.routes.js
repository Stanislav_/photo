(function () {
    "use strict";

    angular
        .module('app')
        .config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider
            .when('/', {
                templateUrl: 'view/app/home/home.jade',
                controller: 'HomeController'
            })
            .when('/login/:msg', {
                templateUrl: 'view/app/login/login.jade',
                controller: 'LoginController'
            })
            .when('/login', {
                templateUrl: 'view/app/login/login.jade',
                controller: 'LoginController'
            })
            .when('/register', {
                templateUrl: 'view/app/register/register.jade',
                controller: 'RegController'
            })
            .when('/profile', {
                templateUrl: 'view/app/user/user.jade',
                controller: 'UserController'
            })
            .when('/usersListView', {
                templateUrl: 'view/app/user/usersList.jade',
                controller: 'UsersListController'
            })
            .when('/album', {
                templateUrl: 'view/app/album/album.jade',
                controller: 'AlbumController'
            })
            .when('/album/:id', {
                templateUrl: 'view/app/album/album.view.jade',
                controller: 'AlbumViewController'
            })
            .when('/photo', {
                templateUrl: 'view/app/photo/photo.jade',
                controller: 'PhotoController'
            })
            .when('/photo/upload', {
                templateUrl: 'view/app/photo/photo.add.jade',
                controller: 'PhotoAddController'
            })
            .otherwise({
                redirectTo: '/'
            })
    }
})();