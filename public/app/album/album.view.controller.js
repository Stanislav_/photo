angular.module('app').controller('AlbumViewController', [
    '$scope',
    '$routeParams',
    'AlbumFactory',
    'PhotoFactory',
    function ($scope, $routeParams, Album, Photo) {

        AllAlbumPhoto();

        $scope.addCom = function (photo, com) {
            if (com) {
                Photo.update({id: photo._id}, {comment: com}, function () {
                    setTimeout(AllAlbumPhoto, 1000);
                    delete $scope.comMsg;
                });
            } else {
                $scope.comMsg = 'You did not write a comment!';
            }
        };

        $scope.edit = function(photo) {
            this.$parent.edited = false;
            Photo.update({ id: photo._id }, photo, function(res) {
                if ( res.success )  {
                    $scope.msg = 'Photo is updated';
                    setTimeout(AllAlbumPhoto, 1000);
                    setTimeout(ClearUpdMsg, 3000);
                }
            });
        };

        $scope.delete = function (item) {
            var res = Photo.delete(item, function () {
                if ( res.success )  {
                    setTimeout(AllAlbumPhoto, 1000);
                }
            });
        };

        $scope.albums = Album.query();

        $scope.album = Album.get({id: $routeParams.id}, function () {});

        function AllAlbumPhoto() {
            $scope.photos = Photo.query({albumId: $routeParams.id});
        }

        function ClearUpdMsg() {
            delete $scope.updMsg;
            $scope.$apply();
        }
    }
]);