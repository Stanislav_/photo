angular.module('app').controller('AlbumController', [
    '$scope',
    'AlbumFactory',
    function ($scope, Album) {

        allUserAlbum();

        $scope.save = function (alb) {

            if (alb.title == null) {
                $scope.errMsg = 'You must type Title!';
                setTimeout(ClearMsg, 2000);
            } else {
                Album.save(alb, function(res) {
                    if (res.success) {
                        $scope.msg = res.msg;
                        $scope.newAlb = false;
                        setTimeout(allUserAlbum, 1000);
                        setTimeout(ClearMsg, 2000);
                    } else {
                        $scope.errMsg = 'Erorr!';
                    }
                });
            }
        };

        $scope.edit = function (item) {
            if (item.title == '') {
                $scope.errMsg = 'You must type Title!';
                setTimeout(ClearMsg, 2000);
            } else {
                Album.update({ id: item._id }, item, function(res) {
                    if (res.success) {
                        $scope.msg = res.msg;
                        setTimeout(allUserAlbum, 1000);
                        setTimeout(ClearMsg, 2000);
                    } else {
                        $scope.errMsg = 'Erorr!';
                    }
                });
            }
        };

        $scope.delete = function (item) {
            Album.delete(item, function (res) {
                if (res.success) {
                    $scope.msg = res.msg;
                    setTimeout(allUserAlbum, 1000);
                    setTimeout(ClearMsg, 2000);
                } else {
                    $scope.errMsg = 'Erorr!';
                }
            });
        };

        function allUserAlbum() {
            $scope.albums = Album.query();
        }

        function ClearMsg() {
            delete $scope.errMsg;
            delete $scope.msg;
            $scope.$apply();
        }
    }
]);