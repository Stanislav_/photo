angular.module('app').controller('PhotoAddController', [
    '$scope',
    'Upload',
    'AlbumFactory',
    function ($scope, Upload, Album) {

        $scope.data = {};

        allUserAlbum();

        $scope.submit = function() {
            if ($scope.file) {

                var validation = true;

                var type = $scope.file.type.split('/')[0];
                if (type !== 'image') {
                    validation = false;
                    $scope.errMsg = 'You try upload not image!!!';
                    setTimeout(Clean, 3000)
                }

                if (!$scope.data.album) {
                    validation = false;
                    $scope.errMsg = 'Select the album!';
                    setTimeout(Clean, 3000)
                }

                if (validation) {
                    $scope.upload();
                }
            } else {
                $scope.errMsg = 'Select a photo!';
                setTimeout(Clean, 3000)
            }
        };

        // upload file
        $scope.upload = function () {

            $scope.data.extension = $scope.file.name.split('.').pop().toLowerCase();
            if (!$scope.data.description) $scope.data.description = '';

            //console.log(data);
            Upload.upload({
                url: 'http://localhost:3000/photo/upload',
                method: 'POST',
                file: $scope.file,
                data: $scope.data
            }).then(function (resp) {
                $scope.msg = 'Image has been uploaded.';
                $scope.file = null;
                delete $scope.data;
                setTimeout(Clean, 3000)
            }, function (resp) {
                $scope.errMsg = 'Error status: ' + resp.status;
                setTimeout(Clean, 3000)

            });
        };

        function Clean() {
            delete $scope.errMsg;
            delete $scope.msg;
            $scope.$apply();
        }

        function allUserAlbum() {
            $scope.albums = Album.query();
        }
    }
]);