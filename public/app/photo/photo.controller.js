angular.module('app').controller('PhotoController', [
    '$scope',
    'PhotoFactory',
    'AlbumFactory',
    function ($scope, Photo, Album) {

        allUserPhoto();

        $scope.edit = function(photo) {
            this.$parent.edited = false;
            var res = Photo.update({ id: photo._id }, photo, function() {
                if ( res.success )  {
                    $scope.updMsg = 'Photo is updated';
                    setTimeout(allUserPhoto, 1000);
                    setTimeout(ClearUpdMsg, 3000);
                }
            });
        };

        $scope.delete = function (item) {
            var res = Photo.delete(item, function () {
                if ( res.success )  {
                    setTimeout(allUserPhoto, 1000);
                }
            });
        };

        $scope.addCom = function (photo, com) {
            if (com) {
                Photo.update({ id: photo._id }, {comment: com}, function() {
                    setTimeout(allUserPhoto, 1000);
                    delete $scope.comMsg;
                });
            } else {
                $scope.comMsg = 'You did not write a comment!';
            }
        };

        function allUserPhoto() {
            $scope.photos = Photo.query({userId: true});
        }

        $scope.albums = Album.query();

        function ClearUpdMsg() {
            delete $scope.updMsg;
            $scope.$apply();
        }
    }
]);