angular.module('app').controller('RegController', [
    '$scope',
    '$location',
    'RegFactory',
    function ($scope, $location, Reg) {

        $scope.register = function () {
            Reg.save($scope.usr, function (res) {
                if (res.error) {
                    $scope.errMsg = res.msg;
                } else if (res.success) {
                    $location.path("/login/" + res.msg);
                }
            });
        };
    }
]);