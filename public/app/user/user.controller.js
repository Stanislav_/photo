angular.module('app')
    .controller('UserController', [
        '$scope',
        'Upload',
        'UserProfileFactory',
        function ($scope, Upload, User) {
            refreshList();

            function refreshList() {
                $scope.user = User.get();
            }

            $scope.edit = function (item) {
                if (item.username == null) {
                    alert('You must type Title!');
                    return
                }
                if (item.email == null) {
                    alert('You must type Title!');
                    return
                }
                if (item.name == null) {
                    alert('You must type Title!');
                    return
                }
                User.update({}, item, function() {
                    delete $scope.edited;
                    setTimeout(refreshList, 1000)
                });
            };

            $scope.upload = function (file) {

                if (file) {
                    console.log(file);

                    var validation = true;

                    var type = file.type.split('/')[0];
                    if (type !== 'image') {
                        validation = false;
                        $scope.error = 'You try upload not image!!!';
                        setTimeout(Clean, 3000)
                    }

                    if (validation) {
                        Upload.upload({
                            url: 'http://localhost:3000/photo/avatar',
                            method: 'POST',
                            file: file
                            //data: $scope.data
                        }).then(function (resp) {
                            setTimeout(refreshList, 1000);
                            setTimeout(Clean, 3000)
                        }, function (resp) {
                            $scope.error = 'Error status: ' + resp.status;
                            setTimeout(Clean, 3000)

                        });
                    }
                }
            };

            function Clean() {
                delete $scope.error;
                delete $scope.msg;
                $scope.$apply();
            }
        }
    ])

    .controller('UsersListController', [
        '$scope',
        'UserFactory',
        function ($scope, User) {
            refreshList();

            function refreshList() {
                $scope.users = User.query();
                console.log($scope.users);
            }

            $scope.delete = function (item) {
                User.delete(item, function (res) {
                    if ( res.success )  {
                        $scope.delMsg = 'User is deleted';
                        setTimeout(refreshList, 1000);
                        setTimeout(ClearDelMsg, 2000);
                    }
                });
            };

            function ClearDelMsg() {
                delete $scope.delMsg;
                $scope.$apply();
            }
        }
    ]);