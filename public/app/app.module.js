angular.module('app', ['ngResource', 'ngRoute', 'ngFileUpload']);

angular.module('app').factory('UserFactory', [
    '$resource',
    function ($resource) {
        return $resource('/user/:id', null, {
            'update': {method: 'PUT'}
        });
    }
]);

angular.module('app').factory('UserProfileFactory', [
    '$resource',
    function ($resource) {
        return $resource('/user/userProfile', null, {
            'update': {method: 'PUT'}
        });
    }
]);

angular.module('app').factory('AlbumFactory', [
    '$resource',
    function ($resource) {
        return $resource('/album/:id', null, {
            'update': {method: 'PUT'}
        });
    }
]);

angular.module('app').factory('PhotoFactory', [
    '$resource',
    function ($resource) {
        return $resource('/photo/:id', null, {
            'update': {method: 'PUT'}
        });
    }
]);

angular.module('app').factory('RegFactory', [
    '$resource',
    function ($resource) {
        return $resource('/user/register');
    }
]);