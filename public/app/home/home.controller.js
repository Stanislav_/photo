angular.module('app').controller('HomeController', [
    '$scope',
    'PhotoFactory',
    function ($scope, Photo) {

        allPhoto();

        $scope.addCom = function (photo, com) {
            if (com) {
                Photo.update({id: photo._id}, {comment: com}, function () {
                    setTimeout(allPhoto, 1000);
                    delete $scope.comMsg;
                });
            } else {
                $scope.comMsg = 'You did not write a comment!';
            }
        };

        function allPhoto() {

            var time = Date.now(),
                day = 86400000;
            $scope.photos = [];

            Photo.query(function (data) {
                data.forEach(function (item, i, arr) {
                    var photoDate = Date.parse(item.lastEdit);
                    //console.log(photoDate);
                    if (photoDate >= time - day) {
                        if (item.status == 'public' || item.status == 'publicPlus') {
                            $scope.photos.push(item);
                        }
                    }
                });
            });
        }
    }
]);