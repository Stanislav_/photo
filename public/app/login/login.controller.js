angular.module('app').controller('LoginController', [
    '$scope',
    '$routeParams',
    function ($scope, $routeParams) {
        if ($routeParams.msg) {
            $scope.successMsg = $routeParams.msg;
        }
    }
]);