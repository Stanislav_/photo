var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    title: { type: String, required: true },
    description: String,
    user: { type: Schema.ObjectId, ref: 'User' },
});

schema.set('autoIndex', false);
module.exports = mongoose.model('Album', schema, 'Albums');