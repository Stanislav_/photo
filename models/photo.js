var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    title: String,
    album: { type: Schema.ObjectId, ref: 'Album', required: true },
    user: { type: Schema.ObjectId, ref: 'User' },
    description: String,
    created: { type: Date, default: Date.now },
    lastEdit: Date,
    //comments: [{ body: String, date: Date, user: { type: Schema.ObjectId, ref: 'User' } }],
    comments: Array,
    status: { type: String, default: 'public' },
    views: { type: Number, default: 0 }
});

schema.set('autoIndex', false);
module.exports = mongoose.model('Photo', schema, 'Photos');